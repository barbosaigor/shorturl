package redirect

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
)

// Server implemented an HTTP API
type Server struct {
	Port uint32
	mux  *http.ServeMux
}

var (
	// ErrNotOkStatus error when HTTP status code is not between 200 and 299
	ErrNotOkStatus = errors.New("Something goes wrong, not ok status")
	// ErrNotFound error when HTTP status code is not between 400 and 499
	ErrNotFound = errors.New("HTTP status Not Found")
	// ErrInternalServerError error when HTTP status code is not between 500 and 599
	ErrInternalServerError = errors.New("HTTP status Internal Server Error")
	// URLHost url endpoint
	URLHost = "http://0.0.0.0:5000"
)

func fetchJSON(method, url string, body io.Reader) (string, error) {
	client := http.Client{}
	request, err := http.NewRequest(method, url, body)
	if err != nil {
		return "", err
	}
	request.Header.Set("Content-Type", "application/json")
	resp, err := client.Do(request)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	if resp.StatusCode == http.StatusOK || resp.StatusCode == http.StatusCreated {
		b, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return "", err
		}
		return string(b), nil
	} else if resp.StatusCode >= 400 || resp.StatusCode < 500 {
		return "", ErrNotFound
	} else if resp.StatusCode >= 500 || resp.StatusCode < 600 {
		return "", ErrInternalServerError
	}
	return "", ErrNotOkStatus
}

func fetchURL(hash string) (string, error) {
	body, err := fetchJSON("GET", URLHost+"/"+hash, nil)
	if err != nil {
		log.Println(err)
		return "", err
	}
	var reqBody struct {
		Original string `json:"original"`
	}
	if err = json.Unmarshal([]byte(body), &reqBody); err != nil {
		log.Println(err)
		return "", err
	}
	return reqBody.Original, nil
}

func handleRedirect(w http.ResponseWriter, r *http.Request) {
	reqURL := r.URL.Path[1:]
	if reqURL == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if r.Method == "GET" {
		url, err := fetchURL(reqURL)
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		http.Redirect(w, r, url, http.StatusPermanentRedirect)
	} else {
		w.WriteHeader(http.StatusNotFound)
	}
}

// NewServer creates the Server instance
func NewServer(port uint32) Server {
	srv := Server{port, http.NewServeMux()}
	srv.mux.HandleFunc("/", handleRedirect)
	return srv
}

// Serve hosts HTTP API on port s.Port
func (s Server) Serve() {
	log.Println("Listening on port:", s.Port)
	http.ListenAndServe(fmt.Sprintf(":%v", s.Port), s.mux)
}
