package keygenerator

import (
	"fmt"
	"log"
	"net/http"
)

// Server implemented an HTTP API
type Server struct {
	Port uint32
	mux  *http.ServeMux
}

func handleKeyGenerate(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		w.Header().Add("Content-Type", "application/json")
		res := []byte(fmt.Sprintf(`{"key": "%s"}`, generateKey()))
		w.Write(res)
	} else {
		w.WriteHeader(http.StatusNotFound)
	}
}

// NewServer creates the Server instance
func NewServer(port uint32) Server {
	srv := Server{port, http.NewServeMux()}
	srv.mux.HandleFunc("/", handleKeyGenerate)
	return srv
}

// Serve hosts HTTP API on port s.Port
func (s Server) Serve() {
	log.Println("Listening on port:", s.Port)
	http.ListenAndServe(fmt.Sprintf(":%v", s.Port), s.mux)
}
