package main

import (
	"keygenerator"
)

func main() {
	s := keygenerator.NewServer(5004)
	s.Serve()
}
