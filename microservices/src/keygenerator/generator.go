package keygenerator

import (
	"crypto/sha1"
	"encoding/base64"
	"time"
)

func generateKey() string {
	hasher := sha1.New()
	hasher.Write([]byte(time.Now().String()))
	return base64.URLEncoding.EncodeToString(hasher.Sum(nil))
}
