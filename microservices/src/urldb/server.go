package urldb

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

var data Data = make(Data)

// Server implemented an HTTP API
type Server struct {
	Port uint32
	mux  *http.ServeMux
}

func handleStore(w http.ResponseWriter, r *http.Request) {
	requestURL := r.URL.Path[1:]
	if requestURL == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	switch r.Method {
	case "GET":
		url, ok := data[requestURL]
		if !ok {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		w.Header().Add("Content-Type", "application/json")
		res := []byte(fmt.Sprintf(`{"Original": "%s"}`, url))
		w.Write(res)
	case "POST":
		var reqJSON struct {
			Hash string `json:"hash"`
		}
		dec := json.NewDecoder(r.Body)
		if dec.Decode(&reqJSON) != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		data[reqJSON.Hash] = requestURL
	default:
		w.WriteHeader(http.StatusNotFound)
	}
}

// NewServer creates the Server instance
func NewServer(port uint32) Server {
	srv := Server{port, http.NewServeMux()}
	srv.mux.HandleFunc("/", handleStore)
	return srv
}

// Serve hosts HTTP API on port s.Port
func (s Server) Serve() {
	log.Println("Listening on port:", s.Port)
	http.ListenAndServe(fmt.Sprintf(":%v", s.Port), s.mux)
}
