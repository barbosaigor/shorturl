package keychaindb

import (
	"fmt"
	"log"
	"net/http"
)

var data *Stack

// Server implemented an HTTP API
type Server struct {
	Port uint32
	mux  *http.ServeMux
}

func handleStore(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		if data.Length <= 0 {
			for i := 0; i < cap(data.container); i++ {
				k, err := fetchKey()
				if err != nil {
					w.WriteHeader(http.StatusInternalServerError)
					return
				}
				data.Push(k)
			}
		}
		w.Header().Add("Content-Type", "application/json")
		res := []byte(fmt.Sprintf(`{"key": "%s"}`, data.Pop().(string)))
		w.Write(res)
	default:
		w.WriteHeader(http.StatusNotFound)
	}
}

// NewServer creates the Server instance
func NewServer(port uint32) Server {
	srv := Server{port, http.NewServeMux()}
	srv.mux.HandleFunc("/", handleStore)
	data = NewStack(10)
	return srv
}

// Serve hosts HTTP API on port s.Port
func (s Server) Serve() {
	log.Println("Listening on port:", s.Port)
	http.ListenAndServe(fmt.Sprintf(":%v", s.Port), s.mux)
}
