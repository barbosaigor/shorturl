package main

import (
	"keychaindb"
)

func main() {
	s := keychaindb.NewServer(5005)
	s.Serve()
}
