package keychaindb

import (
	"testing"
)

func TestPush(t *testing.T) {
	v := NewStack(3)
	v.Push("entry1")
	v.Push("entry2")
	if v.Length != 2 {
		t.Errorf("Push: container Length should be 2 but got %v", v.Length)
	}
	if cap(v.container) != 3 {
		t.Errorf("Push: container capacity should be 3 but got %v", cap(v.container))
	}
	v.Push("entry3")
	if v.Length != 3 {
		t.Errorf("Push: container Length should be 3 but got %v", v.Length)
	}
}

func TestPop(t *testing.T) {
	v := NewStack(3)
	v.Push("entry1")
	v.Push("entry2")
	v.Push("entry3")
	entry := v.Pop()
	t.Log(entry)
	if v.Length != 2 {
		t.Errorf("Push: container Length should be 2 but got %v", v.Length)
	}
	if cap(v.container) != 3 {
		t.Errorf("Push: container capacity should be 3 but got %v", cap(v.container))
	}
	entry = v.Pop()
	t.Log(entry)
	entry = v.Pop()
	t.Log(entry)
	if v.Length != 0 {
		t.Errorf("Push: container Length should be 0 but got %v", v.Length)
	}
}
