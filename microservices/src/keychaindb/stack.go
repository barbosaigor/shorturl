package keychaindb

import "errors"

// Stack is an abstract vector datastructure
type Stack struct {
	container []interface{}
	Length    uint32
}

// NewStack creates a vector instance
func NewStack(size int) *Stack {
	return &Stack{make([]interface{}, size), 0}
}

// Push adds entry to the end of vector
func (v *Stack) Push(entry interface{}) {
	if int(v.Length) < len(v.container) {
		v.container[v.Length] = entry
		v.Length++
	}
}

// Pop retrive first element of vector
func (v *Stack) Pop() interface{} {
	if v.Length <= 0 {
		return errors.New("Empty vector")
	}
	v.Length--
	return v.container[v.Length]
}
