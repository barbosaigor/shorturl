package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"
	"urlcleanup"
)

func main() {
	ticker := time.NewTicker(time.Second)
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)
	for {
		select {
		case <-ticker.C:
			log.Println("Tick")
			urlcleanup.Clean()
		case <-quit:
			log.Println("urlcleanup quit")
			return
		}
	}
}
