package url

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

// Server implements HTTP API
type Server struct {
	port uint32
	mux  *http.ServeMux
}

func handleURL(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		requestURL := r.URL.Path[1:]
		if requestURL == "" {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		url, err := fetchURL(requestURL)
		if err == ErrNotOkStatus || err == ErrInternalServerError {
			w.WriteHeader(http.StatusInternalServerError)
			return
		} else if err == ErrNotFound {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		w.Header().Add("Content-Type", "application/json")
		res := fmt.Sprintf(`{"hash":"%s","original":"%s"}`, url.Hash, url.Original)
		w.Write([]byte(res))
	case "POST":
		dec := json.NewDecoder(r.Body)
		var reqJSON struct {
			Original string `json:"original"`
		}
		err := dec.Decode(&reqJSON)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		originalURL := reqJSON.Original
		url, err := createURL(originalURL)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		err = fetchStoreURL(*url)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.Header().Add("Content-Type", "application/json")
		res := fmt.Sprintf(`{"hash":"%s","original":"%s"}`, url.Hash, url.Original)
		w.Write([]byte(res))
	default:
		w.WriteHeader(http.StatusNotFound)
	}
}

// NewServer creates a server instance
func NewServer(port uint32) Server {
	s := Server{port, http.NewServeMux()}
	s.mux.HandleFunc("/", handleURL)
	return s
}

// Serve hosts an HTTP API for url operations
func (s Server) Serve() {
	log.Println("Listening on port:", s.port)
	http.ListenAndServe(fmt.Sprintf(":%v", s.port), s.mux)
}
