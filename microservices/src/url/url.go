package url

// ErrURL wraps an url error
type ErrURL struct {
	Err error
}

// URL store information about an URL related data
type URL struct {
	Hash     string
	Original string
}

func createURL(original string) (*URL, error) {
	hash, err := fetchHash()
	if err != nil {
		return nil, err
	}
	return &URL{hash, original}, nil
}
