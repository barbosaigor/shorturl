package url

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"net/http"
)

var (
	// ErrURLDBNotResponding error to url db service not responding
	ErrURLDBNotResponding = errors.New("url-db service is not responding")
	// ErrKeychainNotResponding error to keychain service not responding
	ErrKeychainNotResponding = errors.New("keychain service is not responding")
	// ErrNotOkStatus error when HTTP status code is not between 200 and 299
	ErrNotOkStatus = errors.New("Something goes wrong, not ok status")
	// ErrNotFound error when HTTP status code is not between 400 and 499
	ErrNotFound = errors.New("HTTP status Not Found")
	// ErrInternalServerError error when HTTP status code is not between 500 and 599
	ErrInternalServerError = errors.New("HTTP status Internal Server Error")
	// URLDBHost store url db service endpoint
	URLDBHost = "http://0.0.0.0:5001"
	// KeychainHost store keychain service endpoint
	KeychainHost = "http://0.0.0.0:5002"
)

// ErrServiceNotResponding wraps an error when service is not responding
type ErrServiceNotResponding struct {
	Err error
	Msg string
}

func (err ErrServiceNotResponding) Error() string {
	return err.Msg + "\n(Underlying error) " + err.Err.Error()
}

// fetchJSON method types
const (
	POST = "POST"
	GET  = "GET"
)

// fetchJSON request status
const (
	OK = uint8(iota)
	InternalServerError
	NotFound
	Other
)

func fetchJSON(method, url string, body io.Reader) (string, error) {
	client := http.Client{}
	request, err := http.NewRequest(method, url, body)
	if err != nil {
		return "", err
	}
	request.Header.Set("Content-Type", "application/json")
	resp, err := client.Do(request)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	if resp.StatusCode == http.StatusOK || resp.StatusCode == http.StatusCreated {
		b, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return "", err
		}
		return string(b), nil
	} else if resp.StatusCode >= 400 || resp.StatusCode < 500 {
		return "", ErrNotFound
	} else if resp.StatusCode >= 500 || resp.StatusCode < 600 {
		return "", ErrInternalServerError
	}
	return "", ErrNotOkStatus
}

// fetchHash fetches a hash
func fetchHash() (string, error) {
	strHash, err := fetchJSON(GET, KeychainHost, nil)
	if err != nil {
		return "", err
	}
	var key struct {
		Key string `json:"key"`
	}
	err = json.Unmarshal([]byte(strHash), &key)
	if err != nil {
		return "", err
	}
	return key.Key, nil
}

// fetchStoreURL store url to url-db
func fetchStoreURL(url URL) error {
	urlReq := struct {
		Hash string `json:"hash"`
	}{url.Hash}
	data, err := json.Marshal(&urlReq)
	if err != nil {
		return err
	}
	_, err = fetchJSON(POST, URLDBHost+"/"+url.Original, bytes.NewReader(data))
	if err == ErrNotOkStatus {
		return err
	} else if err != nil {
		return ErrServiceNotResponding{err, ErrURLDBNotResponding.Error()}
	}
	return nil
}

// fetchURL fetches url from url-db by a hash
func fetchURL(hash string) (*URL, error) {
	strURL, err := fetchJSON(GET, URLDBHost+"/"+hash, nil)
	if err != nil {
		return nil, err
	}
	var url struct {
		Original string `json:"original"`
	}
	err = json.Unmarshal([]byte(strURL), &url)
	if err != nil {
		return nil, err
	}
	return &URL{Hash: hash, Original: url.Original}, nil
}
