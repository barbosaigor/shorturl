package keychain

import (
	"errors"
	"io"
	"io/ioutil"
	"net/http"
)

var (
	// ErrNotOkStatus error when HTTP status code is not between 200 and 299
	ErrNotOkStatus = errors.New("Something goes wrong, not ok status")
	// ErrNotFound error when HTTP status code is not between 400 and 499
	ErrNotFound = errors.New("HTTP status Not Found")
	// ErrInternalServerError error when HTTP status code is not between 500 and 599
	ErrInternalServerError = errors.New("HTTP status Internal Server Error")
	// KeychainDBHost endpoint
	KeychainDBHost = "http://0.0.0.0:5005"
)

func fetchJSON(method, url string, body io.Reader) (string, error) {
	client := http.Client{}
	request, err := http.NewRequest(method, url, body)
	if err != nil {
		return "", err
	}
	request.Header.Set("Content-Type", "application/json")
	resp, err := client.Do(request)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	if resp.StatusCode == http.StatusOK || resp.StatusCode == http.StatusCreated {
		b, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return "", err
		}
		return string(b), nil
	} else if resp.StatusCode >= 400 || resp.StatusCode < 500 {
		return "", ErrNotFound
	} else if resp.StatusCode >= 500 || resp.StatusCode < 600 {
		return "", ErrInternalServerError
	}
	return "", ErrNotOkStatus
}

func fetchKey() (string, error) {
	return fetchJSON("GET", KeychainDBHost, nil)
}
