package keychain

import (
	"fmt"
	"log"
	"net/http"
)

// Server implemented an HTTP API
type Server struct {
	Port uint32
	mux  *http.ServeMux
}

func handleStore(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		w.Header().Add("Content-Type", "application/json")
		reqBody, err := fetchKey()
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.Write([]byte(reqBody))
	} else {
		w.WriteHeader(http.StatusNotFound)
	}
}

// NewServer creates the Server instance
func NewServer(port uint32) Server {
	srv := Server{port, http.NewServeMux()}
	srv.mux.HandleFunc("/", handleStore)
	return srv
}

// Serve hosts HTTP API on port s.Port
func (s Server) Serve() {
	log.Println("Listening on port:", s.Port)
	http.ListenAndServe(fmt.Sprintf(":%v", s.Port), s.mux)
}
