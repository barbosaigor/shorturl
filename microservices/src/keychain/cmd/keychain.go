package main

import (
	"keychain"
)

func main() {
	s := keychain.NewServer(5002)
	s.Serve()
}
